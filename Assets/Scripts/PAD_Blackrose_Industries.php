<?php 
	
	$group_A = array();
	$group_B = array();
	$group_C = array();
	$group_D = array();
	$group_E = array();
	
	$group_Red = array();
	$group_Blue = array();
	$group_Green = array();
	
	$dungeon_schedule = array();
	$event_schedule = array();
	
	$daily_bonus = array();
	
	
	//get webpage
	$html = file_get_contents('http://www.puzzledragonx.com/');
	
	//print($html);
	
	//get stuff after the first table
	$daily_table = explode('</table>', $html);
	//[1] - metal schedule
		//[2] - subtable for metal schedule contents
	//[3] - "how do i use this information sub info"
	//[4] - group table
		//[ ] - could be a sub group table under this 
	//[5] - tomorrows metal schedule
		//[6] - subtable for metal schedule contents
	//[7] - Recently Added Monsters
		//[8] - subtable for recently added monsters
	//[9] - what's new block
	//[10] - Recently Updated Dungeons
	//[11] - game information
	//[12] - news and features
	//[13] - puzzle and dragons dungeon schedule
	//[14] - puzzle and dragons event schedule
	//[15] - recent community news
	//[16] - todays daily bonus
	
	//print_array($daily_table);


	//metal table********************************************************************************************
	
	//used to assign values to array locations
	$master_index = 0;
	
	//break by table row
	$table_rows = explode('<tr>', $daily_table[figure_out_table_number($daily_table, "metal1a")]);
	//[0] - text after first table
	//[1] - table headers	
	//[2] - A, B, C, D, E of first values
	//[3] - A, B, C, D, E, of first times
	
	//print_array($table_rows);
	
	//break into <td> and parse
		//seperate rows exist for title and time so there are 2 for loops inside this one, the first adds titles to the array, the second adds times to those titles
	for($i = 2; $i < count($table_rows); $i = $i + 2){
		
		//metal table event title****************************************************
		$cells = explode("<td class", $table_rows[$i]);
		
		for($k = 1; $k < count($cells); $k++){
			
			$pos = strpos($cells[$k], " title=");
			
			//in case title isnt used in the code
			if($pos === false){
				
				//check for redirect link and use that
				$pos = strpos($cells[$k], "<a href=");
				if($pos !== false){
					//set to ? if cant find close quote
					$pos_2 = strpos($cells[$k], '" rel=', $pos + 9);
					if($pos_2 === false){
						$title = "?";
					}else{
						$length = ($pos_2) - ($pos + 9);
						$title = substr($cells[$k], $pos + 9, $length);
						//implemented after the dungeon name was removed from the table and replaced with a redirect url
						//print($title."<br />");
						$title = chase_the_white_rabbit($title);
					}
					
					
					//special case: <td class=...link...</td><td class=...(some sort of un-needed empty spacing element)...</td>
					switch($k){
    					case 1:
        					$group_A[$master_index] = $title;
						break;
						case 2:
							$group_A[$master_index] = $group_A[$master_index]." - ".$title;
						break;
						case 3:
       						$group_B[$master_index] = $title;
        				break;
						case 4:
							$group_B[$master_index] = $group_B[$master_index]." - ".$title;
						break;
						case 5:
        					$group_C[$master_index] = $title;
        				break;
						case 6:
							$group_C[$master_index] = $group_C[$master_index]." - ".$title;
						break;
						case 7:
        					$group_D[$master_index] = $title;
        				break;
						case 8:
							$group_D[$master_index] = $group_D[$master_index]." - ".$title;
						break;
						case 9:
        					$group_E[$master_index] = $title;
        				break;
						case 10:
							$group_E[$master_index] = $group_E[$master_index]." - ".$title;
						break;
					}
					
				}else{
					continue;
				}
				
			}
			//
			else{
				$pos_2 = strpos($cells[$k], ">", $pos + 8);
				$length = ($pos_2 - 1) - ($pos + 8);
				$title = substr($cells[$k], $pos + 8, $length); 
			}
			
			switch($k){
    			case 1:
        			$group_A[$master_index] = $title;
        		break;
    			case 2:
       				$group_B[$master_index] = $title;
        		break;
    			case 3:
        			$group_C[$master_index] = $title;
        		break;
				case 4:
        			$group_D[$master_index] = $title;
        		break;
				case 5:
        			$group_E[$master_index] = $title;
        		break;
			}
			
		
		}//end for(cells)
		
		
		//metal table event time*****************************************************
		$cells = explode("<td ", $table_rows[$i+1]);
		
		for($k = 1; $k < count($cells); $k++){
			
			$pos = strpos($cells[$k], ">");
			
			//in case title isnt used in the code
			if($pos === false){
				$title = "?";
			}
			//
			else{
				$pos_2 = strpos($cells[$k], "</td>", $pos + 1);
				$length = ($pos_2 - 1) - ($pos + 1);
				$title = substr($cells[$k], $pos + 1, $length); 
			}
			
			switch($k){
    			case 1:
        			$group_A[$master_index] = $group_A[$master_index]." - ".$title;
        		break;
    			case 2:
       				$group_B[$master_index] = $group_B[$master_index]." - ".$title;
        		break;
    			case 3:
        			$group_C[$master_index] = $group_C[$master_index]." - ".$title;
        		break;
				case 4:
        			$group_D[$master_index] = $group_D[$master_index]." - ".$title;
        		break;
				case 5:
        			$group_E[$master_index] = $group_E[$master_index]." - ".$title;
        		break;
			}
			
		}//end for(cells)
		
		$master_index++;
		
	}//end for(table rows)
	
	
	//group specific dungeon schedule********************************************************************************************
	
	//used to assign values to array locations
	$master_index = 0;
	
	//print($daily_table[4]."<br />");
	
	//break by table row
	$table_number = figure_out_table_number($daily_table, "Tyrra Starter");//red
	if($table_number == NULL){
		$table_number = figure_out_table_number($daily_table, "Plessie Starter");//blue
		if($table_number == NULL){
			$table_number = figure_out_table_number($daily_table, "Brachy Starter");//green
		}
	}
	$table_rows = explode('<tr>', $daily_table[$table_number]);
	//[0] - stuff before
	//[1] - my group selector
	//[2] - monster icons
	//[3] - spacer junk
	//[4] - names
	//[5] - times
	
	$group_Red[$master_index] = "None";
	$group_Blue[$master_index] = "None";
	$group_Green[$master_index] = "None";
	
	$group_order;
	$order_flag = 0;
	
	//break into <td> and parse
	for($i = 1; $i < count($table_rows); $i++){
		
		$cells = explode("<td class", $table_rows[$i]);
		
			for($k = 1; $k < count($cells); $k++){
			
				if(($k == 1) || ($k == 3)){
					$pos = strpos($cells[$k], " title=");
					if($pos !== false){
						$pos_2 = strpos($cells[$k], '">', $pos + 8);
						$length = ($pos_2) - ($pos + 8);
						$starter_string = substr($cells[$k], $pos + 8, $length);
					
						if(strcmp($starter_string, "Tyrra Starter") == 0){
							$starter = 0;
						}
						if(strcmp($starter_string, "Plessie Starter") == 0){
							$starter = 1;
						}
						if(strcmp($starter_string, "Brachy Starter") == 0){
							$starter = 2;
						}
						$group_order[$order_flag] = $starter;
						$order_flag++;
						$k++;
					}
				}
			
				if(($k == 2) || ($k == 4)){
					if($starter != -1){
				
						//check for redirect link and use that
						$pos = strpos($cells[$k], "<a href=");
						if($pos !== false){
							//set to ? if cant find close quote
							$pos_2 = strpos($cells[$k], '" rel=', $pos + 9);
							if($pos_2 !== false){
								$length = ($pos_2) - ($pos + 9);
								$title = substr($cells[$k], $pos + 9, $length);
								//implemented after the dungeon name was removed from the table and replaced with a redirect url
								$title = chase_the_white_rabbit($title);
							}
						}
				
						switch($starter){
    						case 0:
        						$group_Red[$master_index] = $title;
        					break;
    						case 1:
       							$group_Blue[$master_index] = $title;
        					break;
    						case 2:
        						$group_Green[$master_index] = $title;
        					break;
						}
			
					}//end if starter
				
				}//end if k = 2|4
			
			}//end for(cells)
		
	}//end for table rows	
	//print_array($group_order);
	
	//group specific dungeon timing********************************************************************************************
	
	//used to assign values to array locations
	
	//print($daily_table[figure_out_table_number($daily_table, "Schedule is subject to change")]."<br />");
	
	//break by table row
	$table_number = figure_out_table_number($daily_table, "Tyrra Starter");//red
	if($table_number == NULL){
		$table_number = figure_out_table_number($daily_table, "Plessie Starter");//blue
		if($table_number == NULL){
			$table_number = figure_out_table_number($daily_table, "Brachy Starter");//green
		}
	}
	$table_rows = explode('<tr>', $daily_table[$table_number+1]); //subtable that has no identifying marks, just the times
	//[0] - stuff before
	//[1] - my group selector
	//[2] - monster icons
	//[3] - spacer junk
	//[4] - names
	//[5] - times
	
	$case_counter = 0;
	
	//break into <td> and parse
	for($i = 1; $i < count($table_rows); $i++){
	
		$cells = explode("<td colspan", $table_rows[$i]);
		
		if(count($cells) > 1){
			for($k = 1; $k < count($cells); $k++){
			
				$pos = strpos($cells[$k], ">");
				//in case title isnt used in the code
				if($pos === false){
					$title = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</td>", $pos + 1);
					$length = ($pos_2 - 1) - ($pos + 1);
					$title = substr($cells[$k], $pos + 1, $length);
				}
			
				switch($group_order[$case_counter]){
    				case 0:
        				$group_Red[$master_index] = $group_Red[$master_index]." - ".$title;
						$case_counter++;
        			break;
    				case 1:
       					$group_Blue[$master_index] = $group_Blue[$master_index]." - ".$title;
						$case_counter++;
        			break;
    				case 2:
        				$group_Green[$master_index] = $group_Green[$master_index]." - ".$title;
						$case_counter++;
        			break;
				}
			
			}//end for(cells)
		}
	
	}//end for table rows
	
	
	
	
	//special table dungeon schedule********************************************************************************************
	
	//used to assign values to array locations
	$master_index = 0;
	
	//print($daily_table[15]);
	
	//break by table row
	$table_rows = explode('<tr>', $daily_table[figure_out_table_number($daily_table, "Dragons Dungeon Schedule")]);
	//[0] - text before first <tr>
	//[1] - table header	
	//[2] - values...
	
	//break into <td> and parse
		//seperate rows exist for title and time so there are 2 for loops inside this one, the first adds titles to the array, the second adds times to those titles
	for($i = 2; $i < count($table_rows); $i++){
		
		$cells = explode("<td class", $table_rows[$i]);
		
		for($k = 1; $k < count($cells); $k++){
			
			//grab times
			if($k == 1){
				$pos = strpos($cells[$k], "<span class="); //<span class="brown">
				
				//in case title isnt used in the code
				if($pos === false){
					$title = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</span><br>");
					$length = ($pos_2) - ($pos + 20);
					$time = substr($cells[$k], $pos + 20, $length); 
					
					$pos_3 = strpos($cells[$k], "</td>");	
					$length = ($pos_3) - ($pos_2 + 11);
					$title = substr($cells[$k], $pos_2 + 11, $length); 
					$time = $time." - ".$title;
						//print($time."<br />");
				}
				
			}//end grab times
			
			//grab title
			if($k == 2){
			
				$pos = strpos($cells[$k], " title=");
			
				//in case title isnt used in the code
				if($pos === false){
					$title = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], ">", $pos + 8);
					$length = ($pos_2 - 1) - ($pos + 8);
					$title = substr($cells[$k], $pos + 8, $length); 
						//print($title."<br />");
				}
			
			}
			
			//grab active state
			if($k == 4){
			
				$pos = strpos($cells[$k], "The dungeon is active");
			
				//in case title isnt used in the code
				if($pos === false){
					//nothing
				}
				//
				else{
					$dungeon_schedule[$master_index] = $title;
					$master_index++;
					$dungeon_schedule[$master_index] = $time;
					$master_index++;
				}
			
			}
		
		}//end for(cells)
		
	}//end for(table rows)
	
	
	//special table event schedule********************************************************************************************
	
	//used to assign values to array locations
	$master_index = 0;
	
	//print($daily_table[figure_out_table_number($daily_table, "Dragons Event Schedule")]);
	
	//break by table row
	$table_rows = explode('<tr>', $daily_table[figure_out_table_number($daily_table, "Dragons Event Schedule")]);
	//[0] - text before first <tr>
	//[1] - table header	
	//[2] - values...
	//[n] - disclaimer
	
	//break into <td> and parse
		//seperate rows exist for title and time so there are 2 for loops inside this one, the first adds titles to the array, the second adds times to those titles
	for($i = 2; $i < count($table_rows); $i++){
		
		$cells = explode("<td class", $table_rows[$i]);
		
		for($k = 1; $k < count($cells); $k++){
			
			//grab times
			if($k == 1){
				$pos = strpos($cells[$k], "<span class="); //<span class="brown">
				
				//in case title isnt used in the code
				if($pos === false){
					$time = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</span><br>");
					$length = ($pos_2) - ($pos + 20);
					$time = substr($cells[$k], $pos + 20, $length); 
					
					$pos_3 = strpos($cells[$k], "</td>");	
					$length = ($pos_3) - ($pos_2 + 11);
					$title = substr($cells[$k], $pos_2 + 11, $length); 
					$time = $time." - ".$title;
						//print($time."<br />");
				}
				
			}//end grab times
			
			//grab title
			if($k == 3){
			
				$pos = strpos($cells[$k], ">");
			
				//in case title isnt used in the code
				if($pos === false){
					$title = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "<", $pos + 1);
					$length = ($pos_2) - ($pos + 1);
					$title = substr($cells[$k], $pos + 1, $length); 
						//print($title."<br />");
				}
			
			}
			
			//grab active state
			if($k == 4){
			
				$pos = strpos($cells[$k], "The event is active");
			
				//in case title isnt used in the code
				if($pos === false){
					//nothing
				}
				//
				else{
					$event_schedule[$master_index] = $title;
					$master_index++;
					$event_schedule[$master_index] = $time;
					$master_index++;
				}
			
			}
		
		}//end for(cells)
		
	}//end for(table rows)
	
	
	//special table daily bonus*****************************************************************************************************
	
	//used to assign values to array locations
	$master_index = 0;
	
	//print($daily_table[18]);
	
	//break by table row
	$table_rows = explode('<tr>', $daily_table[figure_out_table_number($daily_table, "Today's Daily Bonus")]);
	//[0] - text before first <tr>
	//[1] - table header	
	//[2] - values...
	//[n] - disclaimer
	
	//break into <td> and parse
		//seperate rows exist for title and time so there are 2 for loops inside this one, the first adds titles to the array, the second adds times to those titles
	for($i = 2; $i < count($table_rows); $i++){
		
		$cells = explode("<td class", $table_rows[$i]);
		
		for($k = 1; $k < count($cells); $k++){
			
			//grab times
			if($k == 1){
				$pos = strpos($cells[$k], ">"); 
				
				//in case title isnt used in the code
				if($pos === false){
					$time = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</td>");
					$length = ($pos_2) - ($pos + 1);
					$time = substr($cells[$k], $pos + 1, $length); 
						//print($time."<br />");
				}
				
			}//end grab times
			
			if($k == 2){
				$pos = strpos($cells[$k], ">", 13);
				
				//in case title isnt used in the code
				if($pos === false){
					$title = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</a>");
					$length = ($pos_2) - ($pos + 1);
					$title = substr($cells[$k], $pos + 1, $length); 
						//print($title."<br />");
				}
				
			}//end grab times
			
			//grab description
			if($k == 3){
			
				$pos = strpos($cells[$k], ">");
			
				//in case title isnt used in the code
				if($pos === false){
					$description = "?";
				}
				//
				else{
					$pos_2 = strpos($cells[$k], "</td>");
					$length = ($pos_2) - ($pos + 1);
					$description = substr($cells[$k], $pos + 1, $length); 
						//print($description."<br />");
				}
			
			}
			
		}//end for(cells)
		
		$daily_bonus[$master_index] = $title;
			$master_index++;
		$daily_bonus[$master_index] = $time;
			$master_index++;
		$daily_bonus[$master_index] = $description;
			$master_index++;
		
	}//end for(table rows)
	
	
	
	
	
	
//test prints************************************************************************************************************************	
/*
		print("Group A<br />");
	print_array($group_A);
		print("<br />");
		
		print("Group B<br />");
	print_array($group_B);
		print("<br />");
		
		print("Group C<br />");
	print_array($group_C);
		print("<br />");
		
		print("Group D<br />");
	print_array($group_D);
		print("<br />");
		
		print("Group E<br />");
	print_array($group_E);
		print("<br />");
		
		print("Group Red<br />");
	print_array($group_Red);
		print("<br />");
		
		print("Group Blue<br />");
	print_array($group_Blue);
		print("<br />");
		
		print("Group Green<br />");
	print_array($group_Green);
		print("<br />");
		
		print("Dungeon Schedule - Only pulls events that are currently active<br />");
	print_array($dungeon_schedule);
		print("<br />");
		
		print("Event Schedule - Only pulls events that are currently active<br />");
	print_array($event_schedule);
		print("<br />");

		print("Daily Bonus<br />");
	print_array($daily_bonus);
		print("<br />");
*/
//end test prints************************************************************************************************************************





//test print statement
function print_array($array){
	for($j = 0; $j < count($array); $j++){
		print("[".$j."]: ".$array[$j]."<br />");
	}
}//end print_array

//test print statement
function figure_out_table_number($array, $string){
	for($j = 0; $j < count($array); $j++){
		$pos = strpos($array[$j], $string);
		if($pos === false){
		
		}
		else{
			return $j;
		}
	}
}//end print_array

//called from the Group Dungeon Block
function chase_the_white_rabbit($title){

	//get webpage
	$html = getHTML('http://www.puzzledragonx.com/'.$title, 10);
	
	//get stuff after the first table
	$daily_table = explode("<table", $html);

	//used to assign values to array locations
	$master_index = 0;
	
	//break by table row
	$end_of_table = explode('</table>', $daily_table[figure_out_table_number($daily_table, "<h2>Dungeon</h2>")]);
	
	//break by table row
	$table_rows = explode('<tr>', $end_of_table[0]);
	
	for($i = 1; $i < count($table_rows); $i++){
	
		$pos = strpos($table_rows[$i], '<td class="title value-end nowrap" style="min-width: 100px;">');
			
		//in case title isnt used in the code
		if($pos === false){
			continue;
		}
		//
		else{
			$pos_2 = strpos($table_rows[$i], '</td>', $pos + 61);
			$length = ($pos_2) - ($pos + 61);
			$title = substr($table_rows[$i], $pos + 61, $length); 
				//print($title."<br />");
		}
	
	}
	
	return $title;

}//end chase_the_white_rabbit

//called by chase_the_white_rabbit to follow redirect links and get the html
function getHTML($url,$timeout){
       $ch = curl_init($url); // initialize curl with given url
       curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]); // set  useragent
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // write the response to a variable
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects if any
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); // max. seconds to execute
       curl_setopt($ch, CURLOPT_FAILONERROR, 1); // stop when it encounters an error
       return @curl_exec($ch);
}
?>