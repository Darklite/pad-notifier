﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DungeonDisplay : MonoBehaviour 
{
    public bool updateDisplay = false;
    //public float currentHeight = 0f;
    public RectTransform displaySize;
    public Group groupToDisplay;
    public List<GameObject> allDisplaySets;
    public Main main;

    public GameObject displayObjectTemplate;

    void Start()
    {
        main = GameObject.FindGameObjectWithTag("Main").GetComponent<Main>();
        displaySize = displayObjectTemplate.GetComponent<RectTransform>();
    }

    void Update()
    {
        if(updateDisplay)
        {
            ResetDisplay();

            foreach (TimeSet ts in groupToDisplay.timeGroups)
            {

                //GameObject currentTextDisplay = (GameObject)GameObject.Instantiate(displayObject);
                //allDisplaySets.Add(currentTextDisplay);
                //currentTextDisplay.transform.SetParent(transform, false);
                //currentTextDisplay.transform.Translate(new Vector3(0f , currentHeight, 0f));

                //currentTextDisplay.GetComponentInChildren<UnityEngine.UI.Text>().text = ts.time;
                //foreach (string dungeonName in ts.dungeonNames)
                //{
                //    currentTextDisplay.GetComponentInChildren<UnityEngine.UI.Text>().text += "\n " + dungeonName;
                //}
                //currentHeight -= displaySize.rect.height;
            }

            
        }
    }

    void ResetDisplayVars()
    {
        //currentHeight = 0f;
        updateDisplay = false;
    }

    void ResetDisplay()
    {
        ResetDisplayVars();

        if(allDisplaySets.Count < 1)
        {
            foreach(Group g in main.groups)
            {
                for (int i = 0; i < g.timeGroups.Count; i++ )
                {
                    GameObject currentTextDisplay = (GameObject)GameObject.Instantiate(displayObjectTemplate);
                    allDisplaySets.Add(currentTextDisplay);
                    currentTextDisplay.transform.SetParent(transform, false);

                    currentTextDisplay.GetComponentInChildren<UnityEngine.UI.Text>().text = g.timeGroups[i].time;
                    foreach (string dungeonName in g.timeGroups[i].dungeonNames)
                    {
                        currentTextDisplay.GetComponentInChildren<UnityEngine.UI.Text>().text += "\n " + dungeonName;
                    }

                    allDisplaySets.Add(currentTextDisplay);
                }
            }
            displayObjectTemplate.SetActive(false);
        }

        foreach(GameObject display in allDisplaySets)
        {
            string currentObjectTime = display.GetComponentInChildren<UnityEngine.UI.Text>().text;
            
            foreach(TimeSet ts in groupToDisplay.timeGroups)
            {
                if(currentObjectTime.Equals(ts.time))
                {
                    display.SetActive(true);
                    break;
                }
                display.SetActive(false);
            }
        }
    }

}
