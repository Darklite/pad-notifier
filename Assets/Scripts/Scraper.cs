﻿using UnityEngine;
using System.Collections;

public class Scraper : MonoBehaviour {

    public string testUrl = "http://the-black-rose.com/test_space/PAD_Blackrose_Industries.php";

    
    public string parsedString = "";
    public string ParsedString
    {
        get { return parsedString; }
    }

    public UnityEngine.UI.Text testUIText;

    private bool hasFoundDungeons = false;
    private string wholeString;

    public bool HasFoundDungeons
    {
        get { return hasFoundDungeons; }
    }

	IEnumerator Start () 
    {
        GetComponent<UnityEngine.UI.Text>().text = "SEARCHING...";
        WWW www = new WWW(testUrl);
        yield return www;
    
        wholeString = www.text;
        hasFoundDungeons = true;
        GetComponent<UnityEngine.UI.Text>().text = "FOUND";
        ParseLineBreaks();
	}
	
	//void Start()
    //{
    //    wholeString = GetComponent<UnityEngine.UI.Text>().text;
    //    hasFoundDungeons = true;
    //    ParseLineBreaks();
    //}

    public void ParseLineBreaks()
    {
        int i = 0;
        while(i + 1 < wholeString.Length)
        {
            if(wholeString[i].Equals('<'))
            {
                while(!wholeString[i].Equals('>'))
                {
                    i++;
                }
                parsedString += "\n";
            }
            else
            {
                parsedString += wholeString[i];
            }

            i++;
        }
        //testUIText.text = parsedString;
    }



}
