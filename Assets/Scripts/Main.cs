﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour {

    public bool hasMadeStrings = false;
    public bool hasSecond;
    public string[] groupsAsString;
    public string[] timeTokens = { "[0]:", "[1]:", "[2]:", "[3]:", "[4]:", "[5]:", " - " };
    public char[] timeEndings = { 'a', 'p' };
    public Group[] groups;
    public DungeonDisplay mainDisplay;

    private string parsedString;
    private string[] groupNames = new string[6];
    private Scraper Phil;

	// Use this for initialization
	void Start () 
    {
        SetUpGroups();

        Phil = FindObjectOfType<Scraper>();
	}
	
    void Update()
    {
        if(Phil.HasFoundDungeons && !hasMadeStrings)
        {
            MakeStringGroups();
            MakeTimeGroups();
            hasMadeStrings = true;
        }
    }

	public void SetUpGroups()
    {
        groups[0].groupName = "Group A";
        groups[1].groupName = "Group B";
        groups[2].groupName = "Group C";
        groups[3].groupName = "Group D";
        groups[4].groupName = "Group E";

        for (int i = 0; i < groups.Length; i++)
        {
            groupNames[i] = groups[i].groupName;
            groups[i].IDNumber = i;
        }
    }

    public void MakeStringGroups()
    {
        parsedString = Phil.ParsedString.Substring(0, Phil.ParsedString.IndexOf("Group Red"));
        groupsAsString = parsedString.Split(groupNames, 5, System.StringSplitOptions.RemoveEmptyEntries);
    }

    public void MakeTimeGroups()
    {
        int gASIndex = 0;
        foreach (Group currentGroup in groups)
        {
            string[] groupSplit = groupsAsString[gASIndex].Split(timeTokens, System.StringSplitOptions.None);
            TimeSet tempTimeSet = currentGroup.gameObject.AddComponent<TimeSet>();

            for (int i = 0; i < groupSplit.Length; i++)
            {
                string nameOrTime = groupSplit[i].Trim();
                if (string.IsNullOrEmpty(nameOrTime))
                {
                    continue;
                }
                if (nameOrTime.LastIndexOfAny(timeEndings) == nameOrTime.Length - 1)
                {
                    tempTimeSet.time = nameOrTime;
                    currentGroup.timeGroups.Add(tempTimeSet);
                    if (i != groupSplit.Length - 1)
                    {
                        tempTimeSet = currentGroup.gameObject.AddComponent<TimeSet>();
                    }
                }
                else
                {
                    tempTimeSet.dungeonNames.Add(nameOrTime);
                }
            }
            gASIndex++;
        }
    }

    public void DisplayGroup(Group groupToDisplay)
    {
        mainDisplay.groupToDisplay = groupToDisplay;
        mainDisplay.updateDisplay = true;
        //AndroidNotificationManager.instance.ScheduleLocalNotification("Hello", "This is local notification", 5);

    }

}
